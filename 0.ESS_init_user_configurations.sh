## Do nothing if * doesn't match anithing.
## This is needed for empty directories, otherwise
## "foo/*" expands to the literal string "foo/*/"
shopt -s nullglob

# If current terminal user is not same as the logged-in user, exit the script and do nothing
if [ "$USER" != "$USERNAME" ]; then
  return 1
fi

# Write the logged-in user to a file
echo "USERNAME=$USERNAME" > /ess/.session/local_user

for c in /ess/components/*; do
  if [ -d "$c" ]; then

    HOME_COMPONENT="/home/$USERNAME/.ess/components/${c##*/}"

    if ! [ -d "$HOME_COMPONENT" ]; then
      mkdir -p "$HOME_COMPONENT/"
      for we in /ess/configurations/*; do
        if [ -d "$we" ]; then
          cp -Rf "$c/${we##*/}" "$HOME_COMPONENT/"
        fi
      done
    fi
  fi
done

shopt -u nullglob
